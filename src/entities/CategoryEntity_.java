package entities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * The type Category metamodel.
 */
@StaticMetamodel(CategoryEntity.class)
public class CategoryEntity_ {
    /**
     * The constant id.
     */
    public static volatile SingularAttribute<CategoryEntity, Integer> id;
    /**
     * The constant name.
     */
    public static volatile SingularAttribute<CategoryEntity, String>  name;
}