package entities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.math.BigDecimal;

/**
 * The type Product metamodel.
 */
@StaticMetamodel(ProductEntity.class)
public class ProductEntity_ {
    /**
     * The constant id.
     */
    public static volatile SingularAttribute<ProductEntity, Integer>        id;
    /**
     * The constant name.
     */
    public static volatile SingularAttribute<ProductEntity, String>         name;
    /**
     * The constant price.
     */
    public static volatile SingularAttribute<ProductEntity, BigDecimal>     price;
    /**
     * The constant category.
     */
    public static volatile SingularAttribute<ProductEntity, CategoryEntity> categoryByCategoryId;
}