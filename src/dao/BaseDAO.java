package dao;

/**
 * The interface Base dao.
 *
 * @param <Entity> the type parameter
 */
interface BaseDAO<Entity> {
    /**
     * Find by id of an entity.
     *
     * @param id the id
     * @return the entity
     */
    Entity findById(Integer id);

    /**
     * Find by name list.
     *
     * @param id the id
     * @return the list
     */
    Entity findByName(String id);
}