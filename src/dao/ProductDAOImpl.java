package dao;

import com.sun.istack.internal.Nullable;
import configuration.SessionManager;
import entities.CategoryEntity;
import entities.CategoryEntity_;
import entities.ProductEntity;
import entities.ProductEntity_;

import javax.naming.directory.InvalidAttributesException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Product DAO
 */
public class ProductDAOImpl implements SessionManager, BaseDAO {

    private EntityManager entityManager = getEntityManager();

    /**
     * Find all by params list.
     *
     * @param productName  the product name
     * @param categoryName the category name
     * @param minPrice     the min price
     * @param maxPrice     the max price
     * @return list
     * @throws NoResultException the no result exception
     */
    public List<ProductEntity> findAllByParams(@Nullable String productName, @Nullable String categoryName,
                                               @Nullable BigDecimal minPrice, @Nullable BigDecimal maxPrice)
            throws NoResultException, InvalidAttributesException, NullPointerException {
        CriteriaBuilder                     criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductEntity>        criteria        = criteriaBuilder.createQuery(ProductEntity.class);
        Join<ProductEntity, CategoryEntity> categoryJoin    = null;

        Predicate       minPriceRestriction    = null;
        Predicate       maxPriceRestriction    = null;
        Predicate       productNameRestriction = null;
        List<Predicate> predicates             = new ArrayList<Predicate>();

        Root<ProductEntity> root = criteria.from(ProductEntity.class);
        criteria.select(root);

        if (minPrice != null) {
            minPriceRestriction =
                    criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get(ProductEntity_.price), minPrice));
            predicates.add(minPriceRestriction);
        }

        if (maxPrice != null) {
            maxPriceRestriction =
                    criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get(ProductEntity_.price), maxPrice));
            predicates.add(maxPriceRestriction);
        }

        if (productName != null) {
            productNameRestriction =
                    criteriaBuilder.and(criteriaBuilder.like(root.get(ProductEntity_.name), productName.concat("%")));
            predicates.add(productNameRestriction);
        }

        if (categoryName != null) {
            categoryJoin = root.join(ProductEntity_.categoryByCategoryId);
            categoryJoin.on(criteriaBuilder.like(categoryJoin.get(
                    CategoryEntity_.name), categoryName.concat("%")));
        }

        if (predicates.isEmpty() && categoryJoin == null) {
            throw new InvalidAttributesException("Empty search parameters");
        }

        criteria.where(predicates.toArray(new Predicate[0]));

        List<ProductEntity> result = entityManager.createQuery(criteria).getResultList();

        if (result == null || result.isEmpty()) {
            throw new NoResultException();
        }

        return result;
    }

    public ProductEntity findById(Integer id) throws NoResultException {
        CriteriaBuilder              criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductEntity> criteria        = criteriaBuilder.createQuery(ProductEntity.class);

        Root<ProductEntity> root = criteria.from(ProductEntity.class);
        criteria.select(root);
        criteria.where(criteriaBuilder.equal(root.get(ProductEntity_.id), id));

        ProductEntity result = entityManager.createQuery(criteria).getSingleResult();

        if (result == null) {
            throw new NoResultException();
        }

        return result;
    }

    /**
     * Find all by category id.
     *
     * @param categoryID the category id
     * @return the list
     * @throws NoResultException the no result exception
     */
    public List<ProductEntity> findAllByCategoryId(Integer categoryID) throws NoResultException {
        CriteriaBuilder              criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductEntity> criteria        = criteriaBuilder.createQuery(ProductEntity.class);

        Root<ProductEntity> root = criteria.from(ProductEntity.class);
        criteria.select(root);
        criteria.where(criteriaBuilder.equal(root.get(ProductEntity_.categoryByCategoryId), categoryID));

        List<ProductEntity> result = entityManager.createQuery(criteria).getResultList();

        if (result == null || result.isEmpty()) {
            throw new NoResultException();
        }

        return result;
    }

    public ProductEntity findByName(String name) throws NoResultException {
        CriteriaBuilder              criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductEntity> criteria        = criteriaBuilder.createQuery(ProductEntity.class);

        Root<ProductEntity> root = criteria.from(ProductEntity.class);
        criteria.select(root);
        criteria.where(criteriaBuilder.equal(root.get(ProductEntity_.name), name));

        ProductEntity result = entityManager.createQuery(criteria).getSingleResult();

        if (result == null) {
            throw new NoResultException();
        }

        return result;
    }

    /**
     * Find all by category name.
     *
     * @param name the name
     * @return the list
     * @throws NoResultException the no result exception
     */
    public List<ProductEntity> findAllByCategoryName(String name) throws NoResultException {
        CategoryDAOImpl              categoryDAOImpl = new CategoryDAOImpl();
        CriteriaBuilder              criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductEntity> criteria        = criteriaBuilder.createQuery(ProductEntity.class);

        Root<ProductEntity> root = criteria.from(ProductEntity.class);
        criteria.select(root);
        criteria.where(
                criteriaBuilder.equal(root.get(ProductEntity_.categoryByCategoryId), categoryDAOImpl.findByName(name)));

        List<ProductEntity> result = entityManager.createQuery(criteria).getResultList();

        if (result == null || result.isEmpty()) {
            throw new NoResultException();
        }

        return result;
    }


    /**
     * Find all by price list.
     *
     * @param minPrice the min price
     * @param maxPrice the max price
     * @return the list
     * @throws NoResultException the no result exception
     */
    public List<ProductEntity> findAllByPrice(BigDecimal minPrice, BigDecimal maxPrice) throws NoResultException {
        CategoryDAOImpl              categoryDAOImpl = new CategoryDAOImpl();
        CriteriaBuilder              criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductEntity> criteria        = criteriaBuilder.createQuery(ProductEntity.class);

        Root<ProductEntity> root = criteria.from(ProductEntity.class);
        criteria.select(root);
        criteria.where(criteriaBuilder.between(root.get(ProductEntity_.price), minPrice, maxPrice));

        List<ProductEntity> result = entityManager.createQuery(criteria).getResultList();

        if (result == null || result.isEmpty()) {
            throw new NoResultException();
        }

        return result;
    }
}
