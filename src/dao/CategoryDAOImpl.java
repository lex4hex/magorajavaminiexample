package dao;

import configuration.SessionManager;
import entities.CategoryEntity;
import entities.CategoryEntity_;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Category DAO.
 */
public class CategoryDAOImpl implements SessionManager, BaseDAO {
    private EntityManager entityManager = getEntityManager();

    public CategoryEntity findById(Integer id) throws NoResultException {
        CriteriaBuilder               criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CategoryEntity> criteria        = criteriaBuilder.createQuery(CategoryEntity.class);

        Root<CategoryEntity> root = criteria.from(CategoryEntity.class);
        criteria.select(root);
        criteria.where(criteriaBuilder.equal(root.get(CategoryEntity_.id), id));

        CategoryEntity result = entityManager.createQuery(criteria).getSingleResult();

        if (result == null) {
            throw new NoResultException();
        }

        return result;
    }

    public CategoryEntity findByName(String name) throws NoResultException {
        CriteriaBuilder               criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CategoryEntity> criteria        = criteriaBuilder.createQuery(CategoryEntity.class);

        Root<CategoryEntity> root = criteria.from(CategoryEntity.class);
        criteria.select(root);
        criteria.where(criteriaBuilder.like(root.get(CategoryEntity_.name), name));

        CategoryEntity result = entityManager.createQuery(criteria).getSingleResult();

        if (result == null) {
            throw new NoResultException();
        }

        return result;
    }
}