package controllers;

import dao.ProductDAOImpl;

import javax.naming.directory.InvalidAttributesException;
import javax.persistence.NoResultException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * The type Index controller.
 */
public class CatalogController extends HttpServlet {

    /**
     * Process price big decimal.
     *
     * @param priceString the price string
     * @return the big decimal
     * @throws InvalidAttributesException the invalid attributes exception
     */
    private BigDecimal processPrice(String priceString) throws InvalidAttributesException {
        if (priceString != null && !priceString.isEmpty()) {
            try {
                return new BigDecimal(priceString);
            } catch (NumberFormatException e) {
                throw new InvalidAttributesException("Invalid price value was provided");
            }
        }

        return null;
    }

    /**
     * Process and validate product name parameter
     *
     * @param productName product name from request
     * @return product processed name
     * @throws NullPointerException
     * @throws InvalidAttributesException the invalid attributes exception
     */
    private String processProductName(String productName) throws NullPointerException, InvalidAttributesException {
        if (productName != null && productName.isEmpty()) {
            return null;
        }

        productName = productName.trim();

        return productName;
    }

    /**
     * Process and validate category name parameter
     *
     * @param categoryName category name from request
     * @return category processed name
     * @throws NullPointerException
     * @throws InvalidAttributesException the invalid attributes exception
     */
    private String processCategoryName(String categoryName) throws NullPointerException, InvalidAttributesException {
        if (categoryName != null && categoryName.isEmpty()) {
            return null;
        }

        categoryName = categoryName.trim();

        return categoryName;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get the requestDispatcher and forward the request/response to JSP page
        RequestDispatcher rd = req.getRequestDispatcher("index.jsp");

        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String categoryName   = req.getParameter("categoryName");
        String productName    = req.getParameter("productName");
        String maxPriceString = req.getParameter("maxPrice");
        String minPriceString = req.getParameter("minPrice");

        BigDecimal maxPrice;
        BigDecimal minPrice;

        try {
            // Process string params
            categoryName = processCategoryName(categoryName);
            productName  = processProductName(productName);

            // Process prices
            minPrice = processPrice(minPriceString);
            maxPrice = processPrice(maxPriceString);

            ProductDAOImpl productDAOImpl = new ProductDAOImpl();
            req.setAttribute("products", productDAOImpl.findAllByParams(productName, categoryName, minPrice, maxPrice));
        } catch (InvalidAttributesException e) {
            req.setAttribute("validationError", e.getMessage());
        } catch (NoResultException e) {
            req.setAttribute("noResultMessage", "No data was found by provided params");
        } catch (NullPointerException e) {
            req.setAttribute("validationError", "Invalid search criteria was provided");
        }

        // get the requestDispatcher and forward the request/response to JSP page
        RequestDispatcher rd = req.getRequestDispatcher("index.jsp");

        rd.forward(req, resp);
    }
}
