package configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**
 * The interface Session manager.
 */
public interface SessionManager {
    /**
     * Gets entity manager.
     *
     * @return the entity manager
     */
    default EntityManager getEntityManager() {
        EntityManagerFactory sessionFactory = Persistence.createEntityManagerFactory("MagoraPersistenceUnit");

        return sessionFactory.createEntityManager();
    }
}
