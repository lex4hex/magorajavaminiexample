
### Simple Java example by Alexey Zotov

## Technological stack
- Java 8
- Hibernate 5.2
- JPA API 2.1
- JSP
- Servlet
- Tomcat 8
- MySQL 5.7

## Deploy and configuration
- Import data from dump located in resources/magoraCatalog.sql. It creates database structure with basic test data
- Install tomcat8, then add new user in conf/tomcat-users.xml.
- Add a database connection credentials in conf/context.xml:

```xml
 <Resource name="jdbc/magoraCatalogDS" auth="Container"
            type="javax.sql.DataSource" username="YOUR_USER" password="YOUR_PASSWORD"
            driverClassName="com.mysql.jdbc.Driver" 
            url="jdbc:mysql://HOSTNAME:PORT/magoraCatalog?useUnicode=yes&characterEncoding=UTF-8"
            maxActive="8" 
            />
```

- Log in to tomcat web management panel - localhost/manager/html, 
then locate "WAR file to deploy" and deploy magoraCatalog.war.

- Proceed to given address and enjoy :)

- Little trick: you can enter 1 in minimal price input to check out all available data. 