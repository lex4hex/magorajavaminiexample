<%@ page import="entities.ProductEntity" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head><title>Magora catalog</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>
<%
    String productNameParam = "";

    if (request.getParameter("productName") != null) {
        productNameParam = request.getParameter("productName");
    }

    String categoryNameParam = "";

    if (request.getParameter("productName") != null) {
        categoryNameParam = request.getParameter("categoryName");
    }

    String minPriceParam = "";

    if (request.getParameter("productName") != null) {
        minPriceParam = request.getParameter("minPrice");
    }

    String maxPriceParam = "";

    if (request.getParameter("productName") != null) {
        maxPriceParam = request.getParameter("maxPrice");
    }
%>
<form class="form-horizontal" method="POST">
    <fieldset>
        <!-- Form Name -->
        <legend align="center">Hello, Magora</legend>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="productName">Product name</label>
            <div class="col-md-4">
                <input id="productName" name="productName" type="text" placeholder="Product name"
                       class="form-control input-md" value="<%=productNameParam%>">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="categoryName">Category</label>
            <div class="col-md-4">
                <input id="categoryName" name="categoryName" type="text" placeholder="Category"
                       class="form-control input-md" value="<%=categoryNameParam%>">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="minPrice">Minimal Price</label>
            <div class="col-md-4">
                <input id="minPrice" name="minPrice" type="text" placeholder="minPrice" class="form-control input-md"
                       value="<%=minPriceParam%>">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="maxPrice">Maximal Price</label>
            <div class="col-md-4">
                <input id="maxPrice" name="maxPrice" type="text" placeholder="maxPrice" class="form-control input-md"
                       value="<%=maxPriceParam%>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <button id="submit" class="btn btn-primary " name="submit" type="submit">
                    Submit
                </button>
            </div>
        </div>
    </fieldset>
</form>
<div class="col-md-3 col-md-offset-4">
    <div class="">
        <%
            List<ProductEntity> result = (List<ProductEntity>) request.getAttribute("products");
            String validationError = (String) request.getAttribute("validationError");
            String noResultMessage = (String) request.getAttribute("noResultMessage");
            if (result != null && validationError == null) {
                out.print(
                        "<table class=\"table\"><thead><th>ID</th><th>Product Name</th><th>Price</th><th>Category</th>"
                                + "</thead><tbody>");
                for (ProductEntity productEntity : result) {
                    out.print("<tr><th>" + productEntity.getId() + "</th>");
                    out.print("<td>" + productEntity.getName() + "</td>");
                    out.print("<td>" + productEntity.getPrice() + "</td>");
                    out.print("<td>" + productEntity.getCategoryByCategoryId().getName() + "</td>");
                }
                out.print(" </tbody></table>");
            } else if (validationError != null && !validationError.isEmpty()) {
                out.print("<div class=\"alert alert-danger\"><strong>Warning!</strong> Validation error: " +
                                  validationError + "</div>");
            } else if (noResultMessage != null && !noResultMessage.isEmpty()) {
                out.print("<div class=\"alert alert-info\"><strong>Info! </strong>" + noResultMessage + "</div>");
            }
        %>
    </div>
</div>
</body>
</html>